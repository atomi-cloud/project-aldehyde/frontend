module.exports = function (api) {
    api.cache.never();

    const presets = [[
        "@babel/preset-env",
    ]];
    const plugins = [
        '@babel/plugin-proposal-object-rest-spread',
        ["@babel/plugin-transform-runtime", {regenerator: true}]
    ];

    return {
        sourceType: "unambiguous",
        presets,
        plugins
    };
};