import webpack, {Options} from "webpack";
import SplitChunksOptions = webpack.Options.SplitChunksOptions;

const TerserPlugin = require('terser-webpack-plugin');


// @ts-ignore
const plugins: Array<Plugin | Tapable.Plugin> = [
    new TerserPlugin({
        terserOptions: {
            compress: {
                drop_console: false,
                unsafe: true
            },
            output: {comments: false},
            toplevel: true
        }
    })
]

const chunkOpts: SplitChunksOptions = {
    name: true,
    chunks: 'async',
    // minSize: 20000,
    // maxSize: 250000,
    cacheGroups: {
        vuetify: {
            test: /[\\/]node_modules[\\/]vuetify/,
            priority: 0,
            name: 'vuetify',
            chunks: 'all'
        },
        vue: {
            test: /[\\/]node_modules[\\/]vue/,
            priority: -10,
            name: 'vue',
            chunks: 'all'
        },
        jsonedit: {
            test: /[\\/]node_modules[\\/]v-jsoneditor/,
            priority: -10,
            name: 'json-editor',
            chunks: 'all'
        },
        library: {
            test: /[\\/]node_modules[\\/]/,
            priority: -20,
            name: 'vendors',
            chunks: 'all'
        }
    }
};

function optimize(minimize: boolean, chunkSplit: boolean): Options.Optimization {
    let opti: Options.Optimization = {};
    if (minimize) opti.minimizer = plugins
    if (chunkSplit) opti.splitChunks = chunkOpts
    return opti;
}

export {optimize};