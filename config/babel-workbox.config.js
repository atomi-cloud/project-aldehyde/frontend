module.exports = function (api) {
    api.cache.never();
    const plugins = ['@babel/plugin-proposal-object-rest-spread'];
    return {
        plugins
    };
};