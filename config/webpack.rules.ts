import {RuleSetRule, RuleSetUseItem} from "webpack";
import * as path from "path";
import {prodFileOpts} from "./config.prod";
import {devFileOpts} from "./config.dev";
import {Kore} from "@kirinnee/core";


const core = new Kore();
if (!core.IsExtended)
    core.ExtendPrimitives();


/*===================
 STYLE LOADER
 ===================== */
const preCSSLoaders: RuleSetUseItem[] = [
    {loader: 'postcss-loader', options: {config: {path: `${__dirname}/postcss.config.js`}}},
    {
        loader: "sass-loader",
        options: {
            fiber: require('fiber')
        }
    }
];

const use: RuleSetUseItem[] = [
    "style-loader",
    {loader: "css-loader", options: {importLoaders: preCSSLoaders.length}}
];

const cssRule: RuleSetRule = {
    test: /\.s?css/,
    use: use.concat(preCSSLoaders)
};


/*===================
 BABEL LOADER
 ===================== */
const babelLoader: RuleSetUseItem = {
    loader: 'babel-loader',
    options: {
        configFile: path.resolve(__dirname, "./babel.config.js"),
        // runtimeHelpers: true
    }
}

const babelWorkBoxLoader: RuleSetUseItem = {
    loader: 'babel-loader',
    options: {configFile: path.resolve(__dirname, "./babel-workbox.config.js")}
}


const babel: RuleSetRule = {
    test: /\.jsx?$/,
    exclude: /workbox/,
    use: [
        babelLoader
    ]
};

/*===================
 TS LOADER
 ===================== */

const tsLoader: RuleSetUseItem = {
    loader: 'ts-loader', options: {
        transpileOnly: true,
        appendTsSuffixTo: [/\.vue$/],
        // configFile: path.resolve(__dirname, "../tsconfig.json")
    }
};

const typescript: RuleSetRule = {
    test: /\.tsx?$/,
    exclude: /(node_modules|bower_components|sw\.ts)/,
    use: [
        babelLoader,
        tsLoader
    ]
};

const workbox: RuleSetRule = {
    test: /sw\.ts$/,
    exclude: /(node_modules|bower_components)/,
    use: [
        babelWorkBoxLoader,
        tsLoader
    ]

}


/*===================
 Vue LOADER
 ===================== */

const vue: RuleSetRule = {
    test: /\.vue$/, loader: 'vue-loader',
    options: {hotReload: true}
};

/*===================
 File LOADER
 ===================== */

function file(env: "production" | "development" | "none"): RuleSetRule {
    return {
        exclude: [/\.vue$/, /\.(js|jsx|mjs)$/, /\.(ts|tsx)$/, /\.(scss|css)$/, /\.html$/, /\.json$/],
        loader: 'file-loader',
        options: env === "production" ? prodFileOpts : devFileOpts
    }
}

/* ==============
 Combining
 ================= */

function rules(env: "production" | "development" | "none"): RuleSetRule[] {
    return [
        vue,
        typescript,
        workbox,
        babel,
        cssRule,
        file(env)
    ];
}

export {rules};
