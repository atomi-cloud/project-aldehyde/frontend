import {should} from 'chai';
import {P } from "../../src/classLibrary/Geometry/Point";
import {Core, Kore} from "@kirinnee/core";

should();

const core: Core = new Kore();
core.ExtendPrimitives();

describe("point map", () => {

    describe("mid point", () => {

        it("should compute midpoint", () => {
            const expected1 = P([2, 2]);

            const expected2 = P([0, 1]);
            const expected3 = P([2, 0]);

            const actual1 = P([1, 1]).MidPoint(P([3, 3]));
            const actual2 = P([0, 0]).MidPoint(P([0, 2]));
            const actual3 = P([-1, 0]).MidPoint(P([5, 0]));

            actual1.should.deep.eq(expected1);
            actual2.should.deep.eq(expected2);
            actual3.should.deep.eq(expected3);

        });
    });

    describe("angle", () => {
    	it("should compute the angle to the horizontal axis",()=>{
			P([9, 7]).Angle(P([13, 2])).should.approximately(51.3, 0.1);
			P([9, 7]).Angle(P([4, 6])).should.approximately(168.7, 0.1);
			P([9, 7]).Angle(P([6, 10])).should.approximately(225, 0.1);
			P([9, 7]).Angle(P([13, 10])).should.approximately(323.1, 0.1);
		});
    });

});
