import {PointFResponse} from "./core/pointFResponse";


import * as parse from 'parse-color';
import {Quad, QuadMap} from "./Geometry/Quad";
import {P, Point} from "./Geometry/Point";

interface DrawingConfiguration {

    drawText: boolean;
    drawDangerZone: boolean;
    drawZone: boolean;

    zoneAlpha: number;
    strokeSpacing: number;
    fontSize: number;
    strokeSize: number;
}

interface DrawingOption {
    drawText?: boolean;
    drawDangerZone?: boolean;
    drawZone?: boolean;

    zoneAlpha?: number;
    strokeSpacing?: number;
    fontSize?: number;
    strokeSize?: number;

}

function MapConfig(config: DrawingConfiguration, option: DrawingOption): DrawingConfiguration {
    for (let k in option) {
        if ((option as object).hasOwnProperty(k)) {
            (config as any)[k] = (option as any)[k];
        }
    }
    return config;
}

class Drawer {
    constructor(mapX: number, mapY: number, screenX: number, screenY: number, ctx: CanvasRenderingContext2D) {
        this.mapX = mapX;
        this.mapY = mapY;
        this.screenX = screenX;
        this.screenY = screenY;
        this.ctx = ctx;
    }

    private config: DrawingConfiguration = {
        drawText: true,
        drawZone: true,
        drawDangerZone: true,
        zoneAlpha: 0.2,
        strokeSpacing: 40,
        fontSize: 40,
        strokeSize: 40,
    };

    private mapX: number;
    private mapY: number;
    private readonly screenX: number;
    private readonly screenY: number;
    private readonly ctx: CanvasRenderingContext2D;

    UpdateMap(x: number, y: number): Drawer {
        this.mapX = x;
        this.mapY = y;
        return this;
    }

    Configure(options: DrawingOption) {
        this.config = MapConfig(this.config, options);
    }

    Clear() {
        this.ctx.clearRect(0, 0, this.screenX, this.screenY);
    }

    DiagonalCanvas(): HTMLCanvasElement {

        const pattern = document.createElement("CANVAS") as HTMLCanvasElement;
        const w = this.screenY.AtMin(this.screenX) / this.config.strokeSpacing;
        const h = this.screenX.AtMin(this.screenX) / this.config.strokeSpacing;
        pattern.width = w;
        pattern.height = h;
        const ctx = pattern.getContext("2d")!;
        ctx.strokeStyle = "#FF0000";
        ctx.moveTo(0, w);
        ctx.lineTo(h, 0);
        ctx.stroke();
        return pattern;
    }


    x(x: number): number {
        return x / this.mapX * this.screenX;
    }

    y(y: number): number {
        return y / this.mapY * this.screenY;
    }

    move(p: [number, number]) {
        const [x, y] = p;
        this.ctx.moveTo(this.x(x), this.y(y));
    }

    lineTo(p: [number, number]) {
        const [x, y] = p;
        this.ctx.lineTo(this.x(x), this.y(y));
    }

    Point(a: PointFResponse): [number, number] {
        return [a.x, a.y];
    }

    Fill(style: string | CanvasGradient | CanvasPattern) {
        this.ctx.fillStyle = style;
        this.ctx.fill();
    }

    Stroke(style: string | CanvasGradient | CanvasPattern) {
        this.ctx.strokeStyle = style;
        this.ctx.stroke();
    }

    SimpleDraw(area: Quad) {
        this.ctx.beginPath();
        const [p1, p2, p3, p4] = area;
        this.move(p1);
        this.lineTo(p2);
        this.lineTo(p3);
        this.lineTo(p4);
        this.ctx.closePath();
    }

    NormPoint([x, y]: Point): Point {
        return [this.x(x), this.y(y)];
    }

    CreateGradient(start: Point, end: Point): CanvasGradient {
        const [sx, sy] = this.NormPoint(start);
        const [ex, ey] = this.NormPoint(end);
        return this.ctx.createLinearGradient(sx, sy, ex, ey);
    }

    DrawLine(start: Point, end: Point, width: number, style: string | CanvasGradient | CanvasPattern) {
        this.ctx.beginPath();
        this.move(start);
        this.lineTo(end);
        this.ctx.lineWidth = width;
        this.Stroke(style);
        this.ctx.closePath();
    }

    DrawExit(exit: [Point, Point], color1?: string, color2?: string) {
        const [start, end] = exit;
        const mid = P(start).MidPoint(P(end)).Point;
        const w = this.screenX.AtMin(this.screenY) / this.config.strokeSize;
        const pattern = this.ctx.createPattern(this.DiagonalCanvas(), "repeat")!;
        this.DrawLine(start, mid,w, color1 ??  pattern);
        this.DrawLine(mid, end,w, color2 ?? pattern);
    }

    DrawText(text: string, [x, y]: Point, color: string = 'black', font: string = 'Arial') {

        this.ctx.fillStyle = color;
        this.ctx.strokeStyle = 'black';
        this.ctx.lineWidth = this.screenX.AtMin(this.screenY) / 400;
        this.ctx.textBaseline = 'middle';
        this.ctx.textAlign = 'center';
        this.ctx.font = `${this.screenX.AtMin(this.screenY) / this.config.fontSize}px ${font}`;
        this.ctx.strokeText(text, this.x(x), this.y(y));
        this.ctx.fillText(text, this.x(x), this.y(y));
    }

    DrawArea(quad: Quad, name: string, color: string, danger: boolean) {
        const [r, g, b] = parse(color).rgb;


        console.log(name, this.config.drawText);
        const zone = (new QuadMap(quad)).ClockWise();

        if (this.config.drawZone) {
            this.SimpleDraw(zone.Quad);
            this.Fill(`rgba(${r}, ${g}, ${b}, ${this.config.zoneAlpha})`);
        }

        if (danger && this.config.drawDangerZone) {
            const pattern = this.ctx.createPattern(this.DiagonalCanvas(), "repeat")!;
            this.SimpleDraw(zone.Quad);
            this.Fill(pattern);
        }

        if (this.config.drawText) {
            this.DrawText(name, zone.Center(), color);
        }
    }


}

export {Drawer}