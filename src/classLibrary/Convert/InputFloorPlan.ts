import {InputInfo, InputZone, ZoneMapper} from "./InputZone";
import {ExitMapper, InputExit} from "./InputExit";
import {CreateFloorPlanRequest} from "../core/createFloorPlanRequest";
import {AreaRequest} from "../core/areaRequest";
import {ExitRequest} from "../core/exitRequest";
import {FloorPlanRequest} from "../core/floorPlanRequest";
import {ZoneRequest} from "../core/zoneRequest";


function oMap<T>(o: { [s: string]: T }): Map<string, T> {
    return new Map(Object.entries(o));
}

const FloorPlanMapper = {

    To: function (zones: { [s: string]: InputZone }, exits: { [s: string]: InputExit }, length: number, width: number, image: string, name: string): CreateFloorPlanRequest {

        const zr = oMap(zones)
            .Map((k, v) => {
                const [a, d] = ZoneMapper.To(v, k);
                return [a, d];
            });
        const z = zr.Map(x => x[0] as AreaRequest);
        const d = zr
            .Map(([_, info]) => info as InputInfo);

        const zoneRequests : {[s:string]: ZoneRequest } = {};
        d.Each( z => {
            zoneRequests[z.name] = new ZoneRequest(z.danger,z.color,z.meta);
        });

        const e = oMap(exits).Map((k, v) => ExitMapper.To(v, k) as ExitRequest);
        const floorplan = new FloorPlanRequest(e, z);
        return new CreateFloorPlanRequest(
            floorplan,
            image,
            name,
            zoneRequests,
            length,
            width
        );
    }

};

export {FloorPlanMapper};