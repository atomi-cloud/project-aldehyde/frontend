import {Q, Quad} from "../Geometry/Quad";
import {AreaRequest} from "../core/areaRequest";
import {AreaResponse} from "../core/areaResponse";
import {ZoneResponse} from "../core/zoneResponse";
import {CoordinateMapResponse} from "../core/coordinateMapResponse";
import {CoordinateMapRequest} from "../core/coordinateMapRequest";


type Area = AreaRequest | AreaResponse;
type CoordMap = CoordinateMapResponse | CoordinateMapRequest;


interface InputZone {
    tracked: boolean;
    maps: Quad[];
    danger: boolean;
    meta: { [s: string]: string[] };
    color: string;
}

interface InputInfo {
    danger: boolean;
    meta: { [s: string]: string[] };
    color: string;
    name: string;
}

const ZoneMapper = {

    From: function (zone: Area, zR: ZoneResponse): InputZone {

        const maps = (<CoordMap[]>zone.maps).Map(x => Q(x).Quad);

        return {
            tracked: zone.isTracked,
            danger: zR.danger,
            meta: zR.data as { [s: string]: string[] },
            color: zR.color,
            maps,
        }
    },

    To: function (input: InputZone, name: string): [Area, InputInfo] {

        const out = input.maps.Map(x => Q(x).CoordinateMapRequest);

        return [
            new AreaRequest(name, input.tracked, out),
            {
                danger: input.danger,
                color: input.color,
                meta: input.meta,
                name,
            }
        ]
    }
};


export {InputZone, ZoneMapper, InputInfo}