import {P, Point} from "../Geometry/Point";
import {ExitRequest} from "../core/exitRequest";
import {ExitResponse} from "../core/exitResponse";

interface InputExit {
    a1: string;
    a2: string;
    p: [Point, Point]
}

type Exit = ExitRequest | ExitResponse;

const ExitMapper = {

    From: function (zone: Exit): InputExit {
        return {
            a1: zone.area1,
            a2: zone.area2,
            p: [P(zone.pt1).Point, P(zone.pt2).Point]
        }
    },

    To: function (input: InputExit, name: string): Exit {

        return new ExitRequest(
            name,
            input.a1,
            input.a2,
            P(input.p[0]).PointFRequest,
            P(input.p[1]).PointFRequest
        )
    }
};

export {InputExit, ExitMapper}