/**
 * Core API v1 (OpenAPI: 3.0.1)
 *
 *
 * NOTE: This class is auto generated by openapi-typescript-client-api-generator.
 * Do not edit the file manually.
 */

import {RulePrincipalResponse} from "./rulePrincipalResponse";
import {FloorFlyWeightResponse} from "./floorFlyWeightResponse";

export class ActiveRuleResponse {
    public id: string;

    public rule: RulePrincipalResponse;

    public floor: FloorFlyWeightResponse;

    public subscribedUsers: string[];

    /**
     * Creates a ActiveRuleResponse.
     *
     * @param {string} id
     * @param {RulePrincipalResponse} rule
     * @param {FloorFlyWeightResponse} floor
     * @param {string} subscribedUsers
     */
    constructor(id: string, rule: RulePrincipalResponse, floor: FloorFlyWeightResponse, subscribedUsers: string[]) {
        this.id = id;
        this.rule = rule;
        this.floor = floor;
        this.subscribedUsers = subscribedUsers;
    }
}