/**
 * Core API v1 (OpenAPI: 3.0.1)
 * 
 *
 * NOTE: This class is auto generated by openapi-typescript-client-api-generator.
 * Do not edit the file manually.
 */

import { SubContractorLightResponse } from "./subContractorLightResponse";
import { WorkerLightResponse } from "./workerLightResponse";

export class SubContractorResponse {
    public light: SubContractorLightResponse;

    public workers: WorkerLightResponse[];

    /**
     * Creates a SubContractorResponse.
     *
     * @param {SubContractorLightResponse} light
     * @param {WorkerLightResponse} workers
     */
    constructor(light: SubContractorLightResponse, workers: WorkerLightResponse[]) {
        this.light = light;
        this.workers = workers;
    }
}