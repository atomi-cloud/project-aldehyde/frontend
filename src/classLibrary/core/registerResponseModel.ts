export interface RegisterResponseModel {

    id: string
    sub: string
    username: string

}