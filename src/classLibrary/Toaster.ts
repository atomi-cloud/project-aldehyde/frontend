import toastr from 'toastr';

interface ToastFormat {
    type: 'information' | 'warning' | 'success' | 'error'
    title: string
    message: string
}


class Toaster {


    private readonly registration: ServiceWorkerRegistration | null

    constructor(registration: ServiceWorkerRegistration | null) {
        this.registration = registration;
    }

    async Toast(toast: ToastFormat): Promise<void> {

        if (this.registration != null && Notification.permission == 'granted')
            await this.OutsideToast(toast);

        await this.LocalToast(toast);

    }

    async OutsideToast(toast: ToastFormat): Promise<void> {
        let icon = '';
        switch (toast.type) {
            case "information":
                icon = "info.png"
                break;
            case "warning":
                icon = "warning.png"
                break;
            case "success":
                icon = "success.png"
                break;
            case "error":
                icon = "error.png"
                break;
        }
        const opt = {
            body: toast.message,
            icon,
        }
        await this.registration?.showNotification(toast.title, opt)
    }

    async LocalToast(toast: ToastFormat): Promise<void> {

        const opt = {
            closeButton: true,
            progressBar: true,
            positionClass: window.innerWidth < 600 ? 'toast-bottom-full-width' : 'toast-bottom-right'
        }

        switch (toast.type) {
            case "information":
                toastr.info(toast.message, toast.title, opt);
                break;
            case "warning":
                toastr.warning(toast.message, toast.title, opt);
                break;
            case "success":
                toastr.success(toast.message, toast.title, opt);
                break;
            case "error":
                toastr.error(toast.message, toast.title, opt);
                break;
        }
    }


}

export {ToastFormat, Toaster}