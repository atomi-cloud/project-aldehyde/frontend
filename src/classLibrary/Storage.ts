import {AlertResponse} from "./core/alertResponse";
import {coreApi} from "../pages/index/init";

interface AppStore {
    drawer: boolean;
    loading: boolean;
    tz: string;
    alerts: AlertResponse[];
}

class BStorage {
    value: AppStore;

    constructor(value: AppStore) {
        this.value = value;
    }

    async Dismiss(id: string, user: string, password: string) {
        const r = await coreApi.Delete(`User/${user}/${password}/dismiss/${id}`)
        if (r) {
            await this.UpdateResponse(user, password);
        }
    }

    async UpdateResponse(user: string, password: string) {
        const r = await coreApi.Get<AlertResponse[]>(`User/${user}/${password}/undismissed`);
        if (r.success) {
            this.value.alerts = r.Ok!;
        }
    }
}

export {BStorage, AppStore}