import {DayZoneSubcontractorSecondResponse} from "./analytics/dayZoneSubcontractorSecondResponse";
import {DayZoneSubcontractorWorkerResponse} from "./analytics/dayZoneSubcontractorWorkerResponse";
import {ZoneSubcontractorSecondResponse} from "./analytics/zoneSubcontractorSecondResponse";
import {ZoneSubcontractorWorkerResponse} from "./analytics/zoneSubcontractorWorkerResponse";
import {HourlyZoneSubcontractorWorkerResponse} from "./analytics/hourlyZoneSubcontractorWorkerResponse";
import {HourlyZoneSubcontractorSecondResponse} from "./analytics/hourlyZoneSubcontractorSecondResponse";
import {SortType} from "@kirinnee/core";
import {DaySubcontractorZoneSecondResponse} from "./analytics/daySubcontractorZoneSecondResponse";
import {DaySubcontractorZoneWorkerResponse} from "./analytics/daySubcontractorZoneWorkerResponse";
import {SubcontractorZoneWorkerResponse} from "./analytics/subcontractorZoneWorkerResponse";
import {SubcontractorZoneSecondResponse} from "./analytics/subcontractorZoneSecondResponse";
import {Api} from "./Api";
import {SubContractorResponse} from "./core/subContractorResponse";
import parse from "parse-color";
import {FloorResponse} from "./core/floorResponse";
import {FloorPlanFullResponse} from "./core/floorPlanFullResponse";
import {HourlySubcontractorZoneWorkerResponse} from "./analytics/hourlySubcontractorZoneWorkerResponse";
import {HourlySubcontractorZoneSecondResponse} from "./analytics/hourlySubcontractorZoneSecondResponse";


type Identity = { id: string, name: string };

type HourZoneSub = HourlyZoneSubcontractorWorkerResponse | HourlyZoneSubcontractorSecondResponse;

type HourZoneSubSegment = ZoneSubcontractorSecondResponse
    | ZoneSubcontractorWorkerResponse

type HourSubZone = HourlySubcontractorZoneWorkerResponse | HourlySubcontractorZoneSecondResponse;

type HourSubZoneSegment = SubcontractorZoneSecondResponse
    | SubcontractorZoneWorkerResponse


type ZoneSub =
    DayZoneSubcontractorSecondResponse
    | DayZoneSubcontractorWorkerResponse
    | ZoneSubcontractorSecondResponse
    | ZoneSubcontractorWorkerResponse
type SubZone =
    DaySubcontractorZoneSecondResponse
    | DaySubcontractorZoneWorkerResponse
    | SubcontractorZoneWorkerResponse
    | SubcontractorZoneSecondResponse


type Count = { [s: string]: number };

type Analytics = ZoneSub | SubZone | HourZoneSub | HourSubZone;

type BarChart = [string[], [string, number[]][]];

interface DataSet {
    label: string,
    backgroundColor: string,
    borderColor: string,
    borderWidth: number,
    data: number[]
}

type BarChartDetailed = [string[], DataSet[]]

// zones, hours, value
type HeatMap = [string[], string[], number[][]];


// returns: zone index, subcontractor index,  zones, subs, data
function GenerateDayData(i: Analytics): [Map<string, number>, Map<string, [string, number]>, Count, Count, Map<string, Map<string, number>>] {
    //Re-attempt
    const zonesIdToName = new Map<string, Identity>(Object.entries(i.zones))
        .MapValue(x => x.name as string);

    const zoneNameToIndex = zonesIdToName.Values()
        .Sort(SortType.AtoZ)
        .Unique(true)
        .AsKey((_, i) => i);


    const zones = zonesIdToName.MapValue(v => zoneNameToIndex.get(v)!).AsObject() as Count;

    // id, name
    const subcontractors = new Map(new Map<string, Identity>(Object.entries(i.subcontractors))
        .SortByValue(SortType.AtoZ, f => f.name)
        .MapValue(v => v.name)
        .Arr()
        .Map(([k, v], i) => [k, [v, i]] as [string, [string, number]]));

    const subs = subcontractors.MapValue(([s, v]) => v).AsObject() as Count;

    const data = new Map<string, Count>(Object.entries(i.data))
        .MapValue(x => new Map<string, number>(Object.entries(x)))
    return [zoneNameToIndex, subcontractors, zones, subs, data];
}

function GenerateSegment(val: Count, info: { [s: string]: Identity }): Count {
    const ret: { [s: string]: number } = {}
    for (let k in val) {
        if (val.hasOwnProperty(k)) {
            const v = val[k];
            const rid = info[k].name;
            if (ret[rid] != null) {
                ret[rid] += v;
            } else {
                ret[rid] = v;
            }
        }
    }
    return ret;
}

//zones, subcons, data, hours
function GenerateHourData(i: Analytics): [string[], string[], Map<string, HourZoneSubSegment>, string[]] {
    let zones: string[] = new Map<string, Identity>(Object.entries(i.zones))
        .MapValue(v => v.name as string)
        .Values()
        .Unique()
        .Sort(SortType.AtoZ);
    let subs: string[] = new Map<string, Identity>(Object.entries(i.subcontractors))
        .MapValue(v => v.name as string)
        .Values()
        .Sort(SortType.AtoZ);

    const serverData = new Map<string, HourZoneSubSegment>(Object.entries(i.data))
        .MapKey(k => `${k}:00`);

    const allHours = (serverData).Keys()
        .Map((s: string) => s.ToFloat());
    const max = allHours.Max();
    const min = allHours.Min();
    const hours = [].Fill(max - min, (i) => i + min)
        .Map(h => `${h}:00`);

    return [zones, subs, serverData, hours];
}

export class DataQuery {
    main: Api;
    analytics: Api;

    floor: string;
    date: string;
    value: string;
    trades: string[];
    time: string;
    graph: string;

    constructor(main: Api, analytics: Api, floor: string, date: Date, value: string, trades: string[], time: string, graph: string) {
        this.main = main;
        this.analytics = analytics;
        this.floor = floor;
        this.date = date.toISOString();
        this.value = value;
        this.trades = trades;
        this.time = time;
        this.graph = graph;

    }

    async GetHeatMap(): Promise<HeatMap> {
        const a = await this.QueryData();

        let data = this.graph === 'zone/subcontractor' ?
            AnalyticsParser.HourlyZoneSubToMatrix(a as HourZoneSub)
            : AnalyticsParser.HourlySubZoneToMatrix(a as HourSubZone);

        if (this.value === 'seconds') {
            data = AnalyticsParser.HeatMapToHour(data);
        }
        return data;
    }

    async GetBarChart(): Promise<BarChartDetailed> {
        const a = await this.QueryData();

        let data = this.graph === 'zone/subcontractor' ?
            AnalyticsParser.ZoneSubFormat(a as ZoneSub)
            : AnalyticsParser.SubZoneFormat(a as ZoneSub);

        if (this.value === 'seconds') {
            data = AnalyticsParser.BarChartToHour(data);
        }

        const [zones, raw] = data;
        if (this.graph === 'zone/subcontractor') {
            const dataPromise = raw.Map(async ([id, data]) => {
                const sub = await this.main.Get<SubContractorResponse>(`SubContractor/${id}`);
                if (sub.success) {
                    const s = sub.Ok!;
                    const [r, g, b] = parse(s.light.color).rgb;

                    return {
                        label: s.light.name,
                        backgroundColor: `rgba(${r},${g},${b},0.2)`,
                        borderColor: s.light.color,
                        borderWidth: 1,
                        data,
                    }
                } else {
                    return Promise.reject(sub.error!)
                }
            });
            const d = await Promise.all(dataPromise);
            return [zones, d]
        } else {
            const floorResponse = await this.main.Get<FloorResponse>(`Floor/${this.floor}`);
            const fp = floorResponse.Ok!.latestFloorPlan.id;
            const fpResponse = await this.main.Get<FloorPlanFullResponse>(`FloorPlan/${this.floor}/${fp}`);
            const floorplan = fpResponse.Ok!;
            const map = floorplan.info.zoneData as { [s: string]: { name: string, color: string, danger: boolean } };
            const dataPromise = raw.Map(async ([id, data]) => {
                const area = map[id]
                const [r, g, b] = parse(area.color).rgb;
                return {
                    label: area.name,
                    backgroundColor: `rgba(${r},${g},${b},0.2)`,
                    borderColor: area.color,
                    borderWidth: 1,
                    data
                }
            });
            const d = await Promise.all(dataPromise);
            return [zones, d];
        }

    }


    async QueryData(): Promise<Analytics> {
        const query = this.trades.length > 0 ? `?trades=${this.trades.join(",")}` : ''
        const endpoint = `Analytics/${this.floor}/${this.date}/${this.time}/${this.graph}/${this.value}${query}`;
        const result = await this.analytics.Get(endpoint);
        if (result.success) {
            return result.Ok! as any;
        } else {
            throw new Error("Failed to query");
        }
    }


}

const AnalyticsParser = {


    BarChartToHour: (i: BarChart): BarChart => {
        let [z, sets] = i;
        sets = sets.Map(([id, seconds]) => [id, seconds.Map(x => x / 3600)] as [string, number[]])
        return [z, sets];
    },
    HeatMapToHour(i: HeatMap): HeatMap {
        const [zones, hours, value] = i;
        return [zones, hours, value.Map(r => r.Map(e => e / 3600))];
    },

    HourlySubZoneToMatrix(i: HourSubZone): HeatMap {
        const [, subs, serverData, hours] = GenerateHourData(i);

        const data = Object.fromEntries<Count>(serverData.MapValue(v => this.HourlySubZoneFormat(v)));


        const ret = [].Fill(subs.length, () => [].Fill(hours.length, i => 0));

        subs.Each((s, ri) => {
            hours.Each((h, i) => {
                const v = data[h];
                if (v != null) {
                    if (v[s] != null) {
                        ret[ri][i] = v[s]
                    }
                }
            })
        });
        return [subs, hours, ret];
    },

    HourlySubZoneFormat(i: HourSubZoneSegment): Count {
        let val: Count = {};
        if ((<SubcontractorZoneWorkerResponse>i).workers != undefined) {
            val = (<SubcontractorZoneWorkerResponse>i).workers;
        }
        if ((<SubcontractorZoneSecondResponse>i).seconds != undefined) {
            val = (<SubcontractorZoneSecondResponse>i).seconds;
        }
        return GenerateSegment(val, i.subcontractors);

    },

    HourlyZoneSubToMatrix(i: HourZoneSub): HeatMap {

        const [zones, , serverData, hours] = GenerateHourData(i);

        const data = Object.fromEntries<Count>(serverData.MapValue(v => this.HourlyZoneSubFormat(v)));

        const ret = [].Fill(zones.length, () => [].Fill(hours.length, i => 0));

        zones.Each((z, ri) => {
            hours.Each((h, i) => {
                const v = data[h];
                if (v != null) {
                    if (v[z] != null) {
                        ret[ri][i] = v[z]
                    }
                }
            })
        });
        return [zones, hours, ret];
    },
    // zones, number
    HourlyZoneSubFormat(i: HourZoneSubSegment): Count {

        let val: { [s: string]: number } = {};
        if ((<ZoneSubcontractorWorkerResponse>i).workers != undefined) {
            val = (<ZoneSubcontractorWorkerResponse>i).workers;
        }
        if ((<ZoneSubcontractorSecondResponse>i).seconds != undefined) {
            val = (<ZoneSubcontractorSecondResponse>i).seconds;
        }
        return GenerateSegment(val, i.zones);
    },

    // zone array, array of sub and the value in each zone
    ZoneSubFormat: (i: ZoneSub): BarChart => {

        const [zIndexs, sIndexs, zones, subs, data] = GenerateDayData(i);

        const subConVal = sIndexs
            .Map(id => [id, [].Fill(zIndexs.size, () => 0)] as [string, number[]])

        console.log(subConVal);

        data.Each((zId: string, r) => r.Each((sId: string, v) => {
            const [sIndex, zIndex] = [subs[sId], zones[zId]];
            console.log(sIndex, zIndex);
            subConVal[sIndex][1][zIndex] = v;
        }));
        return [zIndexs.Keys(), subConVal]
    },
    SubZoneFormat: (i: SubZone): BarChart => {

        const [zIndexs, sIndexs, zones, subs, data] = GenerateDayData(i);


        const zoneVal = zIndexs
            .Map(id => [id, [].Fill(sIndexs.size, () => 0)] as [string, number[]]);

        data.Each((sId, r) => r.Each((zId, v) => {
            const [sIndex, zIndex] = [subs[sId], zones[zId]];
            zoneVal[zIndex][1][sIndex] = v;
        }));
        return [sIndexs.Values().Map(([k,]) => k), zoneVal];
    },


}

export {AnalyticsParser}