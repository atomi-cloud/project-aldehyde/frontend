import {Result} from "./Result";

export class Api {
    private readonly host: string;

    constructor(host: string) {
        this.host = host;
    }

    private async parse<T>(v: Response): Promise<Result<T>> {
        if (v.ok) {
            const o: T = await v.json();
            return Result.Ok(o);
        } else {
            const t = await v.text();
            const s = v.status + ": " + t;
            return Result.Err<T>(s);
        }
    }


    public async QPost(path: string, body?: any | null): Promise<string> {
        let init: RequestInit = {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
        };
        if (body != null) {
            init.body = JSON.stringify(body);
        }
        const v = await fetch(`${this.host}/${path}`, init);
        if (v.ok) return "";
        const t = await v.text();
        const s = v.status + ": " + t;
        return s;
    }

    public async PostOrPut<T>(method: 'PUT' | 'POST', path: string, body?: any | null): Promise<Result<T>> {
        try {
            let init: RequestInit = {
                method: method,
                headers: {
                    "content-type": "application/json"
                },
            };
            if (body != null) {
                init.body = JSON.stringify(body);
            }
            const v = await fetch(`${this.host}/${path}`, init);
            return this.parse(v);
        } catch (e) {
            if (e.message) {
                console.error(e)
                return Result.Err(e.message);
            } else {
                return Result.Err(e);
            }
        }
    }

    public Put<T>(path: string, body?: any | null): Promise<Result<T>> {
        return this.PostOrPut('PUT', path, body);
    }

    public async Post<T>(path: string, body?: any | null): Promise<Result<T>> {
        return this.PostOrPut('POST', path, body);
    }

    FullPath(path: string): string {
        return `${this.host}/${path}`;
    }

    public async Delete<T>(path: string): Promise<boolean> {

        const v = await fetch(`${this.host}/${path}`, {method: "DELETE"});
        if (!v.ok) {
            const t = await v.text();
            const s = v.status + ": " + t;
            console.info(s);
        }
        return v.ok;

    }

    public async Get<T>(path: string): Promise<Result<T>> {
        try {

            const v = await fetch(`${this.host}/${path}`);
            return await this.parse(v);
        } catch (e) {
            if (e.message) {
                console.error(e)
                return Result.Err(e.message);
            } else {
                return Result.Err(e);
            }
        }
    }

}