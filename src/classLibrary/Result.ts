export class Result<T> {

    public success: boolean;
    public error?: string;
    public Ok?: T;

    BindDeep<Y>(f: (a: T) => Result<Y>): Result<Y> {
        if (this.success) {
            return f(this.Ok!)
        }
        if (this.error == null) throw new Error("error has no value");
        return Result.Err(this.error);
    }

    Bind<Y>(f: (a: T) => Y): Result<Y> {
        if (this.success) {
            return Result.Ok(f(this.Ok!))
        }
        if (this.error == null) throw new Error("error has no value");
        return Result.Err(this.error);
    }

    static Ok<T>(val: T): Result<T> {
        return new Result<T>(true, null, val)
    }

    static Err<T>(message: string): Result<T> {
        if (message == null) throw new Error("error has no value");
        return new Result<T>(false, message, null)
    }

    constructor(success: boolean, error?: string | null, Ok?: T | null) {
        this.success = success;
        this.error = error!;
        this.Ok = Ok!;
    }


}