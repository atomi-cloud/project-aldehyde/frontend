import Chart from "chart.js";
import moment from "moment";
import 'moment-duration-format';


export function HorizontalBar(canvas: HTMLCanvasElement, valueType: string) {
    return new Chart(canvas,
        {
            type: 'horizontalBar',
            data: {
                labels: [],
                datasets: [],
            },
            options: {
                legend: {
                    display: true
                },
                maintainAspectRatio: false,
                responsive: true,
                tooltips: {
                    mode: 'index',
                    intersect: false,
                    callbacks: {
                        label: function (tooltipItem: any, data: any) {
                            let label: string = data.datasets[tooltipItem.datasetIndex].label || '';
                            let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            if (valueType == "Man Hours") {
                                const seconds = value * 3600;
                                // @ts-ignore
                                const d = moment.duration(seconds, "seconds").format("h [hrs] m [min]");
                                return label + ": " + d;
                            } else {
                                return label + ": " + value.ToInt();
                            }

                        }
                    }
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: {
                            drawTicks: true,
                            drawOnChartArea: true,
                        },
                        ticks: {
                            callback(value: number | string, index: number, values: number[] | string[]): string {
                                if (valueType == "Man Hours") {
                                    return value + "h";
                                } else {
                                    return value.toString();
                                }
                            }

                        },
                        scaleLabel: {
                            display: true
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        gridLines: {
                            drawTicks: true,
                            drawOnChartArea: true,
                        },
                        scaleLabel: {
                            display: true
                        }
                    }]
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
            }
        })
}