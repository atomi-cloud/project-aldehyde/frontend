import {Api} from "./Api";
import {Result} from "./Result";
import {stream} from "../pages/index/init";

enum PollType {
    GET,
    POST,
    QPOST,
    DELETE,
}

class Poller<T> {

    private readonly pollCondition: () => boolean;
    private readonly pollTarget: () => string;
    private readonly pollType: PollType;
    private readonly eventHandler: (t: T) => void;
    private readonly errorHandler: (t: any) => void;
    private readonly delay: number;
    private readonly api: Api;
    private started = false;
    private timeout?: any

    constructor(pollTarget: () => string, pollType: PollType, eventHandler: (t: T) => void, errorHandler: (t: any) => void,
                delay: number, api: Api, pollCondition: () => boolean) {
        this.pollTarget = pollTarget;
        this.eventHandler = eventHandler;
        this.errorHandler = errorHandler;
        this.delay = delay;
        this.api = api;
        this.pollType = pollType;
        this.pollCondition = pollCondition;
    }

    public StartPoll() {
        if (this.started) {
            return;
        }
        this.Poll();
    }

    StopPoll() {
        clearTimeout(this.timeout);
        this.started = false;
    }

    private Poll() {
        this.started = true;
        if (!stream)
            return;

        this.timeout = setTimeout(async () => {
            if (this.pollCondition()) {
                let r: Result<T> | null = null;
                try {
                    switch (this.pollType) {
                        case PollType.GET:
                            r = await this.api.Get<T>(this.pollTarget());
                            break;
                        case PollType.POST:
                            r = await this.api.Post<T>(this.pollTarget());
                            break;
                        case PollType.QPOST:
                            await this.api.QPost(this.pollTarget());
                            break;
                        case PollType.DELETE:
                            await this.api.Delete(this.pollTarget());
                            break;
                    }
                    if (r != null) {
                        if (r.success) {
                            this.eventHandler(r.Ok!)
                        } else {
                            this.errorHandler(r.error)
                        }
                    }
                } catch (e) {
                    this.errorHandler(e);
                }
            }

            this.Poll()
        }, this.delay)
    }

}


export {Poller, PollType}