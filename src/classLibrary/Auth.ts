import {Api} from "./Api";
import {uuid} from 'uuidv4';
import {RegisterResponseModel} from "./core/registerResponseModel";

export class Auth {

    private readonly client: Api;

    constructor(api: Api) {
        this.client = api;
    }

    get UserId(): string {
        return localStorage.getItem("userId") ?? "";
    }

    get Sub(): string {
        return localStorage.getItem("sub") ?? "";
    }

    async logOut() {
        localStorage.removeItem("loggedIn");
        localStorage.removeItem("user");
        localStorage.removeItem("password");
        localStorage.removeItem("sub");
        localStorage.removeItem("userId");
    }

    async signup(username: string, password: string): Promise<string> {

        const r = await this.client.Post<RegisterResponseModel>(`User/register`, {
            password,
            username,
            sub: uuid(),
        });
        if (r.success) {
            return "";
        } else if (r.error! == "Username not available") {
            return r.error!;
        } else {
            try {
                const a = JSON.parse(r.error!) as any;
                if (a.errors.Password)
                    return "Password needs to be at least 8 character long";
                else
                    return a.errors;
            } catch (e) {
                return e;
            }
        }
    }

    async logIn(username: string, password: string): Promise<boolean> {
        const r = await this.client.Post<RegisterResponseModel>(`User/login`, {
            username,
            password,
        });
        if (r.success) {
            const resp = r.Ok!;
            localStorage.setItem("loggedIn", "true");
            localStorage.setItem("user", username);
            localStorage.setItem("password", password);
            localStorage.setItem("sub", resp.sub);
            localStorage.setItem("userId", resp.id);
            return true;
        }
        return false;
    }

    get LoggedIn(): boolean {
        return localStorage.getItem("loggedIn") === "true";
    }

    get Password(): string {
        return localStorage.getItem("password") ?? "";
    }

    get User(): string {
        return localStorage.getItem("user") ?? "";
    }
}