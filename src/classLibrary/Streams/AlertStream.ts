import {AlertResponse} from "../core/alertResponse";
import {Poller, PollType} from "../Poller";
import {Api} from "../Api";
import {Auth} from "../Auth";
import {AlertPrincipalResponse} from "../core/alertPrincipalResponse";

interface AlertStream {

    StartStream(h: (a: AlertResponse) => void): void;

    StopStream(): void;

}

class AlertStreamPoller implements AlertStream {

    poller?: Poller<AlertPrincipalResponse[]>;
    auth: Auth;
    coreApi: Api;
    started: boolean = false;

    constructor(auth: Auth, coreApi: Api) {
        this.auth = auth;
        this.coreApi = coreApi;
    }

    StartStream(h: (a: AlertResponse) => void): void {
        if (this.started) {
            return;
        }
        this.started = true
        const eHandler = (err: object) => {
            console.log(err);
        }

        const actualHandler = function (responses: AlertPrincipalResponse[]) {
            responses.Each(e => h(e.alert));
        }

        this.poller = new Poller<AlertPrincipalResponse[]>(
            () => `User/${this.auth.UserId}/alerts`,
            PollType.POST,
            actualHandler, eHandler, 1000, this.coreApi,
            () => {
                return this.auth.LoggedIn
            }
        )
        this.poller?.StartPoll();
    }

    StopStream(): void {
        this.started = false;
        this.poller?.StopPoll();
    }


}

export {AlertStream, AlertStreamPoller}