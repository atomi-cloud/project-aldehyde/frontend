import Vue from "vue";
import Vuetify from "vuetify/lib";
import 'vuetify/dist/vuetify.min.css'
import {UserVuetifyPreset} from "vuetify";

Vue.use(Vuetify);
const opts: Partial<UserVuetifyPreset> = {
    theme: {
        dark: false
    },
    options: {
        customProperties: true
    },
    icons: {
        iconfont: "mdi",
        values: {
            subscribe: 'icofont-volume-down',
            unsub: 'icofont-volume-mute',
            worker: "icofont-worker",
            dashboard: "icofont-dashboard",
            subcontractor: "icofont-building-alt",
            floor: "icofont-layers",
            logout: "icofont-exit",
            time: "icofont-clock-time",
            del: 'icofont-ui-delete',
            edit: 'icofont-ui-edit',
            show: 'icofont-eye',
            camera: 'icofont-camera-alt',
            color: 'icofont-color-picker',
            circle: "icofont-brand-delicious",
            tag: 'icofont-tags',
            trade: 'icofont-spanner',
            warn: 'icofont-warning-alt',
            analytics: 'icofont-pie-chart',
            graph: 'icofont-chart-bar-graph',
            num: "icofont-mathematical-alt-1",

            label: 'icofont-label',

            code: 'icofont-code-alt',
            triggerType: 'icofont-flash',
            actionType: 'icofont-rounded-double-right',
            desc: 'icofont-file-text',
            rules: 'icofont-ruler-alt-2',
            action: 'icofont-ui-play',
            trigger: 'icofont-automation',
            close: 'icofont-close',
            add: 'icofont-ui-add',
            copy: 'icofont-copy',
            check: 'icofont-check',
            copyi: 'icofont-copy-invert',
            event: 'icofont-calendar',
            download: 'icofont-download',
            footprint: "icofont-foot-print",
        }
    }
};

export default new Vuetify(opts);