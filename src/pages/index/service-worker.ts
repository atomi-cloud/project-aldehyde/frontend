import {UrlB64ToUint8Array} from "../../classLibrary/Base64ToUint8Array";


async function RegisterServiceWorker() {
    if ('serviceWorker' in navigator) {
        const registration = await navigator.serviceWorker.register('/sw.js')
        console.log('SW registered: ', registration);

        const reg = await navigator.serviceWorker.ready;
        let sub = await reg.pushManager.getSubscription();
        if (sub == null) {
            sub = await registration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: UrlB64ToUint8Array('BBb-O-VjwMMS-4VKgTP1i6FC67xAUdDVJR_ktzbloRpOyjVJLLeX3ypOa4tI3twvg2aaaRJVXRitpVBbLdFS_u0')
            });
        }
        console.log(JSON.stringify(sub));
    }
}

//Load Service Worker
async function LoadServiceWorker(box: { prompt: () => void }): Promise<void> {

    // Capure prompt
    window.addEventListener('beforeinstallprompt', (e: Event) => {
        e.preventDefault();
        box.prompt = e as any as (() => void)
    });

    // Request notificaion
    Notification.requestPermission(function (status) {
        console.log('Notification permission status:', status);
    });
    // Register service worker
    window.addEventListener('load', RegisterServiceWorker);

}


export {
    LoadServiceWorker,
}