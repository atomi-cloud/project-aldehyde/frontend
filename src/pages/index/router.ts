import Vue from 'vue'
import Router, {Route, RouteConfig} from 'vue-router'
import About from "./views/About.vue";
import NotFound from "./views/NotFound.vue";
import Auth from "./views/Auth.vue";
import {auth} from "./init";
import EditFloorPlan from "./views/Floors/EditFloorPlan.vue";
import Floor from "./views/Floors/Floor.vue";
import MainHome from "./views/MainHome.vue";
import AllFloor from "./views/Floors/AllFloor.vue";
import AllSubCon from "./views/Subcontractors/AllSubCon.vue";
import CreateSubContractor from "./views/Subcontractors/CreateSubContractor.vue";
import SubContractor from "./views/Subcontractors/SubContractor.vue";
import CreateWorker from "./views/Subcontractors/CreateWorker.vue";
import AllWorker from "./views/Workers/AllWorker.vue";
import Worker from "./views/Workers/Worker.vue";
import Analytics from "./views/Analytics.vue";
import Simulator from "./views/Floors/Simulator.vue";
import QRGenerator from "./views/QRGenerator.vue";
import AlertList from "./views/Alert/AlertList.vue";
import TriggerList from "./views/Alert/TriggerList.vue";
import CreateTrigger from "./views/Alert/CreateTrigger.vue";
import ActionList from "./views/Alert/ActionList.vue";
import CreateAction from "./views/Alert/CreateAction.vue";
import RuleList from "./views/Alert/RuleList.vue";
import CreateRule from "./views/Alert/CreateRule.vue";
import FloorInformation from "./views/Floors/FloorInformation.vue";

Vue.use(Router);

const routes: RouteConfig[] = [
    {path: '/', name: 'home', component: MainHome, meta: {public: false, private: true}},
    {path: '/qrgen', name: 'gr-generator', component: QRGenerator, meta: {public: true, private: true}},
    {path: '/floor', name: 'floors', component: AllFloor, meta: {public: false, private: true}},
    {
        path: '/floor/:id/floorplan/edit',
        name: 'edit-floorplan',
        component: EditFloorPlan,
        meta: {public: false, private: true}
    },
    {
        path: '/floor/:floorId/info',
        name: 'floor-info',
        component: FloorInformation,
        meta: {public: false, private: true}
    },


    {path: '/floor/:id', name: 'floor', component: Floor, meta: {public: false, private: true}},
    {path: '/alert', name: 'alerts', component: AlertList, meta: {public: false, private: true}},

    {path: '/trigger', name: 'triggers', component: TriggerList, meta: {public: false, private: true}},
    {path: '/trigger/create', name: 'create-trigger', component: CreateTrigger, meta: {public: false, private: true}},

    {path: '/action', name: 'actions', component: ActionList, meta: {public: false, private: true}},
    {path: '/action/create', name: 'create-action', component: CreateAction, meta: {public: false, private: true}},


    {path: '/rule', name: 'rules', component: RuleList, meta: {public: false, private: true}},
    {path: '/rule/create', name: 'create-rule', component: CreateRule, meta: {public: false, private: true}},

    {path: '/sim/:id', name: 'sim', component: Simulator, meta: {public: false, private: true}},
    {path: '/sub', name: 'subs', component: AllSubCon, meta: {public: false, private: true}},
    {path: '/sub/create', name: 'create-sub', component: CreateSubContractor, meta: {public: false, private: true}},
    {path: '/worker', name: 'workers', component: AllWorker, meta: {public: false, private: true}},
    {
        path: '/worker/:subconId/:workerId',
        name: 'worker',
        component: Worker,
        meta: {public: false, private: true}
    },
    {path: '/analytics', name: 'analytics-choose', component: Analytics, meta: {public: false, private: true}},
    {path: '/analytics/:id', name: 'analytics', component: Analytics, meta: {public: false, private: true}},
    {path: '/sub/:id', name: 'sub', component: SubContractor, meta: {public: false, private: true}},
    {
        path: '/sub/:id/worker/create',
        name: 'create-worker',
        component: CreateWorker,
        meta: {public: false, private: true}
    },
    {path: '/auth', name: 'auth', component: Auth, meta: {public: true, private: false}},
    {path: '/about', name: 'about', component: About, meta: {public: true, private: true}},

    {path: '*', name: '404', component: NotFound, meta: {public: true, private: true}},

];
const router: Router = new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

//Middle Ware

router.beforeEach(async (to: Route, from: Route, next) => {
    const logged = auth.LoggedIn;
    const pr = to.meta.private;
    const pu = to.meta.public;

    if (pr && pu) return next();
    if (pr && !pu) {
        if (!logged) {
            await router.push("/auth");
            return
        }
    }
    if (!pr && pu) {
        if (logged) {
            await router.push("/");
            return
        }
    }
    if (!pr && !pu) {
        return;
    }
    return next();
});

export {router};
