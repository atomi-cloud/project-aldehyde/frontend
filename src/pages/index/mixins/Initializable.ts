import {Vue, Watch} from "vue-property-decorator";
import {Route} from "vue-router";
import {Mixin} from "vue-mixin-decorator";

@Mixin
export class Initializable extends Vue {

    async created() {
        const x = this as any;

        if (x.onCreate) {
            await x.onCreate();
        }

        if (x.init) {
            await x.init();
        }


    }


    async mounted() {
        const x = this as any;

        if (x.onMount) {
            await x.onMount();
        }

        if (x.mountInit) {
            await x.mountInit();
        }
    }

    @Watch('$route', {immediate: true, deep: true})
    async onUrlChange(_: Route) {
        const x = this as any;
        if (x.init)
            await x.init();
        if (x.mountInit)
            await x.mountInit();
    }
}