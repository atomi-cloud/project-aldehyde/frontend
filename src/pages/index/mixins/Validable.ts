import { Vue} from 'vue-property-decorator';
import {Mixin} from "vue-mixin-decorator";


@Mixin
export class Validable extends Vue{
    success = "";
    error = "";

    get ok() {
        return this.success
    }

    get err() {
        return this.error;
    }

    set ok(s: string) {
        this.success = s;
        this.error = "";
    }


    set err(s: string) {
        this.success = "";
        this.error = s;
    }
}