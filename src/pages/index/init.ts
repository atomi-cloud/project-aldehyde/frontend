import {Core, Kore} from "@kirinnee/core";
import {Api} from "../../classLibrary/Api";
import {Auth} from "../../classLibrary/Auth";
import {Toaster} from "../../classLibrary/Toaster";
import 'toastr/toastr.scss'

const core: Core = new Kore();
core.ExtendPrimitives();


declare var PRODUCTION: boolean;

const coreEndpoint = PRODUCTION ? "https://core.dev.builderlytics.atomi.cloud" : "http://core.localhost:5000";
const analyticsEndpoint = PRODUCTION ? "https://analytics.dev.builderlytics.atomi.cloud" : "http://analytics.localhost:5000";

const coreApi = new Api(coreEndpoint);
const analyticsApi = new Api(analyticsEndpoint);


const auth = new Auth(coreApi);


const triggerType = [
    {value: 'WorkerExit', name: 'Worker Exit', desc: 'When worker enters a zone'},
    {value: 'WorkerEnter', name: 'Worker Enter', desc: 'When worker exits a zone'},
    {value: 'ZoneUpdate', name: 'Zone Update', desc: 'When number of workers in a zone changes'},
    {value: 'WorkerDuration', name: 'Worker Update', desc: 'When worker duration in a zone increases'},
];

const actionType = [
    {value: 'push_notification', name: 'Push Notification', desc: 'Send a push notification'},
    {value: 'send_email', name: 'Send Email', desc: 'Send an email'},
];

const trades = [
    "Aircon Piping Installation",
    "Marine Driven Piling Operation",
    "Aluminium Formwork",
    "Marine Sand Compaction Piling Operation",
    "Asphalt Concrete Paving",
    "Metal Scaffold Erection",
    "Bored Micro-Piling Operation",
    "Metal Scaffold Erection (System Scaffold)",
    "Bored Piling Operation",
    "Metal Scaffold Erection (Tubular Scaffold)",
    "Bricklaying",
    "Mobile Crane Operation",
    "Cladding Installation",
    "Painting",
    "Construction Plant Operation (Bulldozer Operation)",
    "Pipefitting",
    "Construction Plant Operation (Excavator Loader Operation)",
    "Plastering",
    "Construction Plant Operation (Track Shovel Operation)",
    "Plumbing & Pipefitting",
    "Crawler Crane Operation",
    "Precast Concrete Component Erection",
    "Crawler Drill Operation",
    "Precast Concrete Component Erection (with Tower Crane Hoist)",
    "Curtain Wall Installation",
    "Precast Kerb and Drain Laying",
    "Deep Cement Mixing Operation",
    "Precision Blocklaying and Wall Panel Installation",
    "Doors & Windows Installation (Timber)",
    "Prestressing",
    "Doors and Windows Installation (Aluminium)",
    "Soil Drilling & instrumentation",
    "Driven Piling Operation",
    "Steel Reinforcement Work",
    "Ducting Installation for Air-Conditioning and Ventilation",
    "Structural Steel Fitting",
    "Electrical Wiring Installation",
    "Suspended Ceiling Installation (Acoustical)",
    "Enhanced Aluminium Formwork",
    "Suspended Ceiling Installation (Fibrous Plaster)",
    "Enhanced System Formwork",
    "Suspended Scaffold Installation (Mast Climbing Platform)",
    "Enhanced Timber Formwork",
    "Suspended Scaffold Installation (Gondola)",
    "Fibre Optics Installation",
    "System Formwork Installation",
    "Fire Sprinkler Installation",
    "Telescopic Handler Operation",
    "Gas Pipefitting",
    "Thermal Insulation",
    "Glazing",
    "Tiling",
    "Guniting",
    "Timber Flooring",
    "Hydraulic Excavator Operation",
    "Timber Formwork",
    "Hydraulic Excavator Operation (as Lifting Machine)",
    "Tower Crane (Luffing Jib) Operation",
    "Interior Drywall Installation",
    "Tower Crane (Saddle Jib) Operation",
    "Interlocking Blocks Pavement Construction",
    "Trenchless Pipe Installation",
    "Interior Gas Pipefitting",
    "Tunnel Boring Machine (Earth Pressure Method)",
    "Jack-in Piling Operation",
    "Tunnel Boring Machine (Slurry Method)",
    "Jet Grout Piling Operation",
    "Underground Pipe-Jacking",
    "Joinery",
    "Waterproofing",
    "Lift Installation",
    "Welding",
    "Marine Dredging Plant Operation",
    "Mini Crane Operation",
]

const stream = false;

const $$ = (i: number): Promise<void> => new Promise<void>(r => setTimeout(r, i));
const isMobile = (): boolean => window.innerHeight > window.innerWidth;
const isPWA = (): boolean => window.matchMedia('(display-mode: standalone)').matches;

const toaster = new Toaster(null);

export {
    $$,
    isMobile,
    isPWA,
    core,
    auth,
    coreApi,
    analyticsApi,
    trades,
    toaster,
    stream,
    actionType,
    triggerType,
}
