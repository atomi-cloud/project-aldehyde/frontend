import Vue from 'vue';
import App from './App.vue';
import './index.scss';
import {router} from "./router";
import {images} from './images';
import vuetify from "./vuetify";
import VJsoneditor from 'v-jsoneditor';
import PerfectScrollbar from 'vue2-perfect-scrollbar';
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css';
import {LoadServiceWorker} from "./service-worker";


Vue.use(VJsoneditor);
Vue.use(PerfectScrollbar);

Vue.config.productionTip = false;

declare var PRODUCTION: boolean;

const box = {
    prompt: () => {
    }
};

if (PRODUCTION) {
    LoadServiceWorker(box).then(() => console.log("service worker loaded"));
}

new Vue({
    vuetify,
    router,
    render: h => h(App)
}).$mount('#app');

export {
    images,
    box,
}
